import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Primeiro_Projeto',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
